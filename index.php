<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>s01: PHP Basics and Selection Control Structures</title>
</head>
<body>
	<!-- main entry point ay index,
	 -->
	<!-- <h1>Hello World! - rinout values/couput lahat ng variables</h1> -->

	<h1>Echoing Values</h1>
	<!-- print, output variables -->
	<!-- ctrl, shift, d -->
	<p>
		<?php echo 'Good day $name! Your given email is $email' ?>
		
	</p>
	<p>
		<?php echo "Good day $name! Your given email is $email"?>
	</p>
	<p>
		<?php echo PI ?>
	</p>

	<h1>Data Types</h1>
	<p>
		<?php echo $address ?>
		<?php echo $age ?>
	</p>

	<p>
		<?php echo $gradesObj->firstGrading; ?>
		<?php echo $personObj->address->state; ?>		
	</p>

	<!-- normal echoing of null and boolean var will not make it visible to the web page -->
	<!-- not visible in the webpage -->
	<!-- in order to get the type -->
	<p><?php echo $hasTravelledAbroad; ?></p>
	<p><?php echo $girlfriend; ?></p>

	<!-- var_dump see more details info on the variable -->
	<p><?php echo gettype($hasTravelledAbroad) ; ?></p>
	<p><?php echo var_dump($hasTravelledAbroad); ?></p>
	<p><?php echo var_dump($girlfriend); ?></p>

	<p><?php echo $grades[2]; ?></p>
	
	<h1>Operators</h1>

	<h2>Arithmetic Operators</h2>
	<p>Sum: <?php echo $x + $y; ?></p>

	<h2>Equality Operators</h2>

	<p>Loose Equality: <?php echo var_dump($x == '1342.14'); ?></p>
	<p>Loose Equality: <?php echo var_dump($x != '1342.14'); ?></p>

	<h2>Greater/lesser Operators</h2>
	<p>is greater: <?php echo var_dump($x > $y); ?></p>

	<p>Are All Requirements Met: <?php echo var_dump($isLegalAge && $isRegistered); ?></p>
	<p>Are All Requirements Met: <?php echo var_dump(!$isLegalAge && !$isRegistered); ?></p>

	<h1> Function </h1>
	<p>Full Name: <?php echo getFullName('John','D.','Smith'); ?></p>

	<h2>If-Elseif-Else</h2>
	<p><?php echo determineTyphoonIntensity(35); ?></p>

	<h2>Ternary</h2>
	<p>78: <?php var_dump(isUnderAge(78)); ?></p>

	<!-- ctrl enter -->
	<h2>Switch</h2>

	<p><?php echo determineComputerUser(4) ?></p>

	<p><?php echo greeting(404) ?></p>

	<!-- Activity 1 -->

	<h1> Full Address </h1>
	<p><?php echo getFullAddress('Philippines','Tiaong','Quezon','Brgy. Lumingon'); ?></p>
	<p><?php echo getFullAddress('Philippines','Quezon City','Metro Manila','3F Caswynn Bldg., Timog Avenue'); ?></p>
	<p><?php echo getFullAddress('Philippines','Makati City','Metro Manila','3F Enzo Bldg., Buendia Avenue'); ?></p>

	<!-- Activity 2 -->

	<h1>Letter-Based Grading</h1>
	<p><?php echo getLetterGrade(87); ?></p>
	<p><?php echo getLetterGrade(94); ?></p>
	<p><?php echo getLetterGrade(74); ?></p>

</body>
</html>